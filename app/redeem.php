<?php include 'header.php'; ?>

<div class="flwdth-ylw-dw">
	<div class="uk-container">
		<h1> What's In Store For You</h1>
	</div>
</div>

<div class="flwdth">
    <div class="uk-container">
        <div class="uk-width-1 rwng">
<h3>Check out all the Goldilocks goodies you can redeem with your points!<br/>
Be sure to tell us which products you love by clicking on the heart.</h3>

        <div class="uk-hidden-large">
            <form class="uk-form">
                <div class="uk-form-icon uk-width-1">
                    <i class="uk-icon-search"></i>
                    <input type="text" placeholder="search" class="uk-width-1">
                </div>
            </form>
        </div>


            <div class="uk-width-1 rdm uk-clearfix" data-uk-scrollspy="{cls:'uk-animation-fade  uk-invisible', repeat: true, target:'.rdm_cont', delay:200}">
                <div class="rdm_cont  uk-invisible">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="http://www.facebook.com/sharer/sharer.php?u=<?= $_SERVER['SERVER_NAME']?>"   target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" onClick="shareTwitter();"  class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="rdm_cont  uk-invisible">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="rdm_cont  uk-invisible">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="rdm_cont  uk-invisible">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="rdm_cont  uk-invisible">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="rdm_cont  uk-invisible">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="rdm_cont  uk-invisible">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="rdm_cont  uk-invisible">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="rdm_cont  uk-invisible">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>



            </div>

             <div class="uk-width-1 uk-text-center">
                <ul class="paginate">
                    <li>
                        <a href="#">Prev</a>
                    </li>
                    <li>
                        <a href="#">Next</a>
                    </li>
                </ul>
            </div>



        </div>
    </div>
</div>


<?php include 'footer.php'; ?>
