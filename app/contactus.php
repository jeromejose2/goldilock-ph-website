<?php include 'header.php'; ?>

<div class="flwdth-ylw-dw">
	<div class="uk-container">
		<h1> Contact Us</h1>
	</div>
</div>

<div class="flwdth">
    <div class="uk-container">
        <div class="uk-width-1 rwng">
<h3>We’d love to hear from you! If you have any questions or concerns, do feel free to contact us at any of the following:</h3>
        </div>

        <div class="uk-grid">
            <div class="uk-width-large-1-2 brdbrwn">
                <div class="cntc uk-clearfix">
                    <div class="cntc_icn">
                        <img src="assets/img/images/corp-icn_03.png"/>
                    </div>
                    <div class="cntc_desc">
                        <p>
                            Goldilocks Bakeshop <br/> 498 Shaw Boulevard Mandaluyong City
                        </p>
                    </div>
                </div>
                <div class="cntc uk-clearfix">
                    <div class="cntc_icn">
                        <img src="assets/img/images/phne-icn_03.png"/>
                    </div>
                    <div class="cntc_desc">
                        <p>
                           Goldilocks Trunk Line:<br/>
532-4050
                        </p>
                    </div>
                </div>
                <div class="cntc uk-clearfix">
                    <div class="cntc_icn">
                        <img src="assets/img/images/mail-icn_03.png"/>
                    </div>
                    <div class="cntc_desc">
                        <p>
                           E-mail us at <br/>
gtizen@goldilocks.com.ph
                        </p>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-1-2 maps">
                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15444.740854735295!2d121.04214648830587!3d14.588519659708938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c83061981f0f%3A0xff01d782a86b83b6!2sGoldilocks!5e0!3m2!1sen!2sph!4v1433145907176" width="100%" height="450" frameborder="0" style="border:0"></iframe>
                <p class="uk-text-right"><a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15444.740854735295!2d121.04214648830587!3d14.588519659708938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c83061981f0f%3A0xff01d782a86b83b6!2sGoldilocks!5e0!3m2!1sen!2sph!4v1433145907176" target="_blank"><b>View larger Map</b></a></p>
            </div>
        </div>

        <div class="uk-grid frm frmcus">
            <div class="uk-width-large-1-2">

                <form  id="ajax-contact" method="post" action="mailer.php"  class="uk-form uk-clearfix">
                    <div id="form-messages" class="uk-width-1-1"></div>
                    <div class="uk-width-1 uk-clearfix">
                        <label>Name</label>
                        <input type="text" name="fullname">
                    </div>
                    <div class="uk-width-1 uk-clearfix">
                        <label>Contact</label>
                        <input type="text" name="contact">
                    </div>
                    <div class="uk-width-1 uk-clearfix">
                        <label>E-mail</label>
                        <input type="text" name="email">
                    </div>
            </div>
            <div class="uk-width-large-1-2">
                    <div class="uk-width-1 uk-clearfix">
                        <label>Message</label>
                        <textarea name="message"></textarea>
                    </div>
                    <div class="uk-width-1 ctc_btn">

                        <button>Submit</button>
                    </div>

                    </form>
            </div>
        </div>


        <div class="uk-width-1 ctc" style="border-top: 1px dotted #672903; padding-top: 30px;">
            <p>For RCBC My Wallet Visa concerns please contact:</p>
        </div>

        <div class="uk-grid ctc">
            <div class="uk-width-small-1-2 uk-width-large-1-4">
                <p>All About My Wallet:<br/>
<a href="mywallet.php" style="color:#04b3c8">Visit Page</a></p>
            </div>
            <div class="uk-width-small-1-2 uk-width-large-1-4">
                <p>RCBC Customer Contact Center: <br/>
<span style="color:#04b3c8">877-7222</span></p>
            </div>
            <div class="uk-width-small-1-2 uk-width-large-1-4">
               <p> Domestic Toll-Free Hotline:<br/>
<span style="color:#04b3c8">1-800-10000-7222</span></p>
            </div>
            <div class="uk-width-small-1-2 uk-width-large-1-4">
               <p>International Toll-Free Hotline: <br/>
                   <span style="color:#04b3c8">+ 800-8888-7222</span></p>
            </div>
        </div>


    </div>
</div>


<?php include 'footer.php'; ?>
