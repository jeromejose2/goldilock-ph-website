<?php include 'header.php'; ?>

<div class="flwdth-ylw-dw">
	<div class="uk-container">
		<h1> Privileges</h1>
	</div>
</div>

<div class="flwdth">
    <div class="uk-container">
        <div class="uk-width-1 rwng uk-invisible" data-uk-scrollspy="{cls:'uk-animation-slide-left uk-invisible', delay:300, repeat: true}">
            <h1 class="hd-rib">
                <span>benefits</span><span></span>
            </h1>
            <ol class="lstpd">
               <li> The Gtizen cardholder will be able to earn points with every purchase from any Goldilocks store nationwide. <a href="">Terms and conditions</a> apply.</li>
<li>
The card is also a VISA-powered multi-function card allowing you to use the card in thousands of other establishments within the
    country and abroad.

            </li>
                </ol>
        </div>
        
        <div class="uk-width-1 rwng uk-invisible" data-uk-scrollspy="{cls:'uk-animation-slide-left uk-invisible', delay:600, repeat: true}">
            <h1 class="hd-rib">
                <span>Perks & advantages</span><span></span>
            </h1>
            
            <div class="uk-grid prkad">
                <div class="uk-width-small-1-2">
                    <img src="assets/img/images/perks_03.jpg"/>
                </div>
                <div class="uk-width-small-1-2">
                    <ul>
                        <li>No maintaining balance required.</li>
                        <li> With all the benefits available to regular ATM accountholders.</li>
                        <li> Accepted globally where VISA is available.</li>
                        <li>The card has multi-functions:
                        
                            <ul>
                                <li>Cash withdrawal</li>
                                <li>Balance inquiry and balance transfer</li>
                                <li>Bill payment</li>
                                <li>Can be used for shopping or purchase</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="uk-width-1 rwng uk-invisible" data-uk-scrollspy="{cls:'uk-animation-slide-left uk-invisible', delay:900, repeat: true}">
            <h1 class="hd-rib">
                <span>Values</span><span></span>
            </h1>
            <ol class="lstpd">
                 <li>
           Every P25.00 is equivalent to 1 point. Points can then be used to purchase products from any Goldilocks store.
</li>
             <li>
Comes with a pre-loaded P100 Birthday Treat (100 points)
</li>
             <li>
Activated My Wallet VISA card pre-loaded with P20.00.
</li>
             <li>
Among the first to receive regular updates on Goldilocks promotions, new product launches, events and exciting activities for the whole family.
</li>
            <li>
The Visa debit card will allow customers to use our card in thousands of other establishments within the country and abroad
            </li>
            </ol>
        </div>
        
    </div>
</div>

<?php include 'footer.php'; ?>