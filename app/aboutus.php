<?php include 'header.php'; ?>

<div class="flwdth-ylw-dw">
	<div class="uk-container">
		<h1> About Gtizen</h1>
	</div>
</div>

<div class="flwdth">
	<div class="uk-container abt">
		<div class="uk-grid">
            <div class="uk-width-small-1-2">
                <img src="assets/img/images/grl_model_03_03.png"/>
            </div>
            <div class="uk-width-small-1-2">
                <h2>
                For only P150, valued Goldilocks customers will enjoy the exciting benefits of being a GTIZEN My Wallet Visa cardholder.
                </h2>
                <p>
                    Functions as our own Customer Retention Management (CRM) program and a Debit Card powered altogether.
<br/><br/>
A VISA-powered multi-function card allowing customers to use the card in thousands of other establishments within the country and abroad.
                </p>
            </div>
        </div>
        
	</div>
    
    <div class="uk-container">
         <h1 class="hd-rib">
            <span>HOW WIll the card work?</span><span></span>
        </h1>
    </div>
    
    
    
    <div class="uk-container abt">
        <div class="uk-grid">
            <div class="uk-width-small-1-2">
                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-medium-1-2">
                        <img src="assets/img/images/crd_frnt_11.png"/>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <p class="pdtp">
                            The chip in front of the card will contain all details about the customer's Gtizen card
membership.
                        </p>
                    </div>
                </div>
            </div>
            <div class="uk-width-small-1-2" style="padding-top: 0">
                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-medium-1-2">
                        <img src="assets/img/images/crd_bck_11.png"/>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <p class="pdtp">
                        The magnetic strip at the back of the card will contain their My Wallet card information.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="uk-width-1 uk-margin-top">
            <small><i>*Both Gtizen card and My Wallet function will work independently from each other.</i></small>
        </div>
        
    </div>
    
</div>


<?php include 'footer.php'; ?>