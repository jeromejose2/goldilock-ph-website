// toggle menu script tags
$(".uk-navbar-toggle").click(function() {
    //$("this").style.content = "\f16d";
    $('.navbar-toggle i').addClass('uk-icon-close');
    $("ul.uk-navbar-nav").toggle(300);
});

$(".tog-acc").click(function() {
    $("ul.tog-acc_menu").toggle(200);
    $("ul.uk-navbar-nav").hide(200);
});


// carousel homepage script tags

$(document).ready(function() {

    var owl = $("#home-slider");

    owl.owlCarousel({
        items: 1, //10 items above 1000px browser widthitemsDesktop : [1000,1], //5 items between 1000px and 901px
        singleItem: true,
        autoPlay: true,
        pagination: true
    });

    // Custom Navigation Events
    $(".next").click(function() {
        owl.trigger('owl.next');
    })
    $(".prev").click(function() {
        owl.trigger('owl.prev');
    })
    $(".play").click(function() {
        owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
    })
    $(".stop").click(function() {
        owl.trigger('owl.stop');
    })

});



// my account favorites script tags

$(document).ready(function() {

    var owl = $("#fvrts");

    owl.owlCarousel({
        items: 4, //10 items above 1000px browser width
        itemsDesktop: [1000, 4], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 4], // betweem 900px and 601px
        itemsTablet: [768, 2], //2 items between 600 and 0
        itemsMobile: [320, 1] // itemsMobile disabled - inherit from itemsTablet option
    });

    // Custom Navigation Events
    $(".fvrtnext").click(function() {
        owl.trigger('owl.next');
    })
    $(".fvrtprev").click(function() {
        owl.trigger('owl.prev');
    })

});

// my account catalogue script tags

$(document).ready(function() {

    var owl = $("#rdm");

    owl.owlCarousel({
        items: 4, //10 items above 1000px browser width
        itemsDesktop: [1000, 4], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 4], // betweem 900px and 601px
        itemsTablet: [768, 2], //2 items between 600 and 0
        itemsMobile: [320, 1] // itemsMobile disabled - inherit from itemsTablet option
    });

    // Custom Navigation Events
    $(".rdmnext").click(function() {
        owl.trigger('owl.next');
    })
    $(".rdmprev").click(function() {
        owl.trigger('owl.prev');
    })

});


$('.uk-accordion h1 span').click(function() {
    $("html, body").animate({
        scrollTop: 0
    }, 600);
});


$(function() {
    // Get the form.
    var form = $('#ajax-contact');

    // Get the messages div.
    var formMessages = $('#form-messages');

    $(form).submit(function(event) {
        // Stop the browser from submitting the form.
        event.preventDefault();
        var formData = $(form).serialize();
        var el = $('.preloader');
        el.fadeIn(300);
        $.ajax({
            type: 'POST',
            url: $(form).attr('action'),
            data: formData
        }).done(function(response) {
            // Make sure that the formMessages div has the 'success' class.
            $(formMessages).removeClass('error');
            $(formMessages).addClass('success');
            el.fadeOut(300);

            // Set the message text.
            $(formMessages).text(response);

            // Clear the form.
            $('#name').val('');
            $('#email').val('');
            $('#message').val('');
        }).fail(function(data) {
            // Make sure that the formMessages div has the 'error' class.
            $(formMessages).removeClass('success');
            $(formMessages).addClass('error');
            el.fadeOut(300);
            // Set the message text.
            if (data.responseText !== '') {
                $(formMessages).text(data.responseText);
            } else {
                $(formMessages).text('Oops! An error occured and your message could not be sent.');
            }
        });
    });

    // TODO: The rest of the code will go here...
});
