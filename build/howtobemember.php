<?php include 'header.php'; ?>

<div class="flwdth-ylw-dw">
	<div class="uk-container">
		<h1> How to be a member</h1>
        
        <div class="mbr_cnt uk-flex">
            <div class="mbr_pnl  uk-invisible" data-uk-scrollspy="{cls:'uk-animation-slide-left uk-invisible', delay:300, repeat: true}">
                <div class="mbr_num">1</div>
                <div class="mbr_icn">
                    <img src="assets/img/images/form-icn_03.png" />
                </div>
                <div class="mbr_desc">
                    <p>
                        Get a Gtizen Application form from a Goldilocks store near you.
                    </p>
                </div>
            </div>
            
            <div class="mbr_pnl  uk-invisible" data-uk-scrollspy="{cls:'uk-animation-slide-bottom uk-invisible', delay:600, repeat: true}">
                <div class="mbr_num">2</div>
                <div class="mbr_icn">
                    <img src="assets/img/images/id-icn_05.png" />
                </div>
                <div class="mbr_desc">
                    <p>
                       Fill up the application form and submit a photo bearing valid ID together with your P300.00 payment.
                    </p>
                </div>
            </div>
            
            <div class="mbr_pnl  uk-invisible" data-uk-scrollspy="{cls:'uk-animation-slide-right uk-invisible', delay:900, repeat: true}">
                <div class="mbr_num">3</div>
                <div class="mbr_icn">
                    <img src="assets/img/images/env_icn_07.png" />
                </div>
                <div class="mbr_desc">
                    <p>
                       An email update will be sent to inform you when your Gtizen card is ready for pick up from the Goldilocks store you applied from.
                    </p>
                </div>
            </div>
        </div>
        
	</div>
</div>


<div class="flwdth">
    <div class="uk-container">
        <div class="uk-width-1 rwng uk-invisible" data-uk-scrollspy="{cls:'uk-animation-slide-left uk-invisible', delay:300, repeat: true}">
            <h1 class="hd-rib">
                <span>terms & conditions</span><span></span>
            </h1>
            <h3>
                How do I earn points?
            </h3>
            <ul class="lstpd">
                <li>
                Present card upon payment. No card, no point credit. No manual entry of card number will be allowed.
                </li>
                <li>No points can be earned during the waiting period between application and release of the permanent card.</li>
                <li>P25.00 purchase = P1.00</li>
                <li>Any amount below the P25.00 increment will not earn any point (e.g. Purchase of P48.00 will only earn 1 point: 
     P25.00=1 and P18.00=0).</li>
                <li>Wholesale purchases with wholesale discount (Retailers) will not be valid for any points.</li>
                <li>Earning of points is not applicable for Bulk Orders at store level. The Gtizen card cannot be used in conjunction with special 
     packages or discounted transactions.</li>
                <li>In case of lost cards, cardholders cannot earn points until the release of new card. Points earned in the lost card shall remain.</li>
                <li>Purchases using GTIZEN POINTS shall earn NO point.</li>
           <li>For Senior Citizens, PWD, Employee, etc., points apply after the discount.</li>  
                <li>Earned points will expire with the card. Any points earned within the 5-year membership should be consumed / used within the 
     membership period.</li>
                <li>The Gtizen card can only be used exclusively at Goldilocks stores in the Philippines. Purchase from Goldilocks stores abroad are 
    not subject to earn points.</li>
                <li>No points can be earned in events where the store is unable to connect to the database by force majure (e.g. power shortage).</li>
                <li> In case of lost cards, earned points shall remain intact provided that the cardholder reports about the loss to RCBC hotline and 
     that no claiming of points transpired prior to the report.</li>
            </ul>
            
            <h3>What is the P100 birthday treat?</h3>
            
            <ul class="lstpd">
                <li>The Gtizen card will be pre-loaded with 100 points, which the customer can only use during his/her birthday month on the first year 
    of his Gtizen membership.</li>
                <li>Cardholder can claim his birthday treat upon presentation of his Gtizen card.</li>
                <li>The P100 Birthday Treat is applicable on a one-time purchase of any Bakeshop or Foodshop product worth P100.00, either dine in or take out.</li>
                <li>In excess of P 100.00 purchase, cardholder will pay the excess amount.</li>
                <li>In case of purchases less than P100.00, no change will be issued to cardholder.</li>
                <li>Birthday treat can be claimed only ONCE. The system will decline any succeeding availment.</li>
            </ul>
            
            <h3>How long is the validity of the card?</h3>
            
            <ul class="lstpd">
                <li>The card is valid for 5 years from the date of application and may be used at any Goldilocks store nationwide. To renew, the 
     member must fill out a renewal form from any Goldilocks store 1 month before the expiry of the card.</li>
        </div>
    </div>
</div>
    

<?php include 'footer.php'; ?>