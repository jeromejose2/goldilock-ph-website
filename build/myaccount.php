<?php include 'header.php'; ?>

<div class="flwdth-ylw-dw">
    <div class="uk-container">
        <h1>My Account</h1>
    </div>
</div>

<div class="flwdth">
    <div class="uk-container">
        <div class="uk-width-1 rwng">
<h4>Keep track of all the points you’ve earned and spent, and all the Goldilicious sweets you’ve redeemed.<br/>
You can also change your password here by clicking on the Change Password button.</h4>

            <h1 class="hd-rib">
                <span>Member's Profile</span><span></span>
            </h1>

            
            <div class="uk-grid"> 
                <div class="uk-width-medium-1-3 uk-text-center">
                    <div class="prf_img" style="background: url('assets/img/images/prfl_img_03.jpg')">
                        <img src="assets/img/images/prfl_img_03.jpg"/>
                    </div>
                    <div class="prf_btns">
                        <button>upload button</button>
                        <br/> <i>OR</i> <br/>
                        <button class="fb">facebook sign in</button>
                    </div>
                </div>
                
                <div class="uk-width-medium-2-3">
                    <form class="uk-form">   
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-1-3 mbr_prfl-lbl">
                            <p>NAME</p>
                            <p class="addrss">MAILING ADDRESS</p>
                            <p>BIRTHDAY</p>
                            <p>MOBILE NUMBER</p>
                            <p>EMAIL ADDRESS</p>
                            <p>CARD NUMBER</p>
                            <p>STORE APPLIED FROM</p>
                        </div>
                        <div class="uk-width-2-3 mbr_prfl-inf">
                            <p>Jessie Aspiras</p>
                            <p class="addrss">1209 Vellagio Tower Leon Guinto St. Quirino
Avenue Malate, Manila</p>
                            <p>November 10, 1988</p>
                            <p>09178363600</p>
                            <p>jessie.aspiras@nuworks.ph</p>
                            <p>0 003 000002055 0</p>
                            <p>TAFT</p>
                        </div>
                        
                             <div class="uk-width-1" style="margin: 20px 0">                
                                <div class="uk-width-1-3 mbr_prfl-lbl">
                                   <h4 style="color: #02b1c6;">CHANGE PASSWORD</h4>
                                </div>
                            </div>
                        
                            <div class="uk-width-medium-1-3 mbr_prfl-lbl">
                                <label>old password</label>
                            </div>
                            <div class="uk-width-medium-2-3 mbr_prfl-inf">
                                <input type="password" class="uk-width-1"/>
                            </div>
                        
                            <div class="uk-width-medium-1-3 mbr_prfl-lbl">
                                <label>new password</label>
                            </div>
                            <div class="uk-width-medium-2-3 mbr_prfl-inf">
                                <input type="password" class="uk-width-1"/>
                            </div>
                        
                            <div class="uk-width-medium-1-3 mbr_prfl-lbl">
                                <label>verify password</label>
                            </div>
                            <div class="uk-width-medium-2-3 mbr_prfl-inf">
                                <input type="password" class="uk-width-1"/>
                            </div>
                       
                        
                        
                        
                        
                        
                    </div>
                    
                     </form>

                    
                    
                </div> 
                
                
                
            </div>
            
            
             
            
            <h1 class="hd-rib">
                <span>Update</span><span></span>
            </h1>
            

            
                <div class="uk-width-1 accpd" style="margin-bottom: 50px;">
                    <h4 style="color: #02b1c6">REMINDER</h4>
                    <p>You earn 1 point for every corresponding P25.00 purchase increments made in any Goldilocks store nationwide.<br/>
Any amount below the P25.00 increment will not earn any point.</p>
                
                    <div class="uk-grid uk-grid-small">
                        
                        <div class="uk-width-medium-1-5">
                            <p><strong>Points as of <br/>January 21, 2015</strong></p>
                        </div>
                        <div class="uk-width-medium-4-5">
                            <div class="pnts">
                                <div class="pnl_1">
                                    POINTS <br/>EARNED
                                </div>
                                <div class="pnl_2">
                                    240
                                </div>
                            </div>
                            <div class="pnts">
                                <div class="pnl_1">
                                    POINTS <br/>REDEEMED
                                </div>
                                <div class="pnl_2">
                                    0
                                </div>
                            </div>
                            <div class="pnts">
                                <div class="pnl_1">
                                    POINTS <br/>BALANCED
                                </div>
                                <div class="pnl_2">
                                    240
                                </div>
                            </div>
                        </div>
                        
                    </div>                
                </div>
                
                <div class="uk-width-1 accpd" style="margin-bottom: 50px;">
                    <h4 style="color: #02b1c6">HISTORY</h4>
                    
                    <table class="uk-table">
                        <thead>
                            <th class="uk-text-center">DATE OF REDEEM</th>
                            <th class="uk-text-center">ITEM</th>
                            <th class="uk-text-center">POINTS</th>
                            <th class="uk-text-center">BRANCH</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
            
            <hr/>
                
                <div class="uk-width-1 accpd" style="margin-top: 50px;">
                    <h4 style="color: #02b1c6">FAVORITES</h4>
                    
                    <div class="fvrts uk-clearfix">
                        <ul>
                            <div id="fvrts" class="owl-carousel owl-theme">
                            <li class="item">
                                <div class="fvrts_img uk-clearfix" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                                    <img src="assets/img/images/rdm_img_03.jpg"/>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_pts">
                                    <span>150 pts.</span>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_desc">
                                    <span class="title">Lorem Ipsum dolor sit</span>
                                    <p>
                                       Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    </p>
                                </div>
                                
                            </li>
                                
                                <li class="item">
                                <div class="fvrts_img uk-clearfix" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                                    <img src="assets/img/images/rdm_img_03.jpg"/>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_pts">
                                    <span>150 pts.</span>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_desc">
                                    <span class="title">Lorem Ipsum dolor sit</span>
                                    <p>
                                       Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    </p>
                                </div>
                                
                            </li>
                                
                                <li class="item">
                                <div class="fvrts_img uk-clearfix" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                                    <img src="assets/img/images/rdm_img_03.jpg"/>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_pts">
                                    <span>150 pts.</span>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_desc">
                                    <span class="title">Lorem Ipsum dolor sit</span>
                                    <p>
                                       Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    </p>
                                </div>
                                
                            </li>
                                
                                <li class="item">
                                <div class="fvrts_img uk-clearfix" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                                    <img src="assets/img/images/rdm_img_03.jpg"/>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_pts">
                                    <span>150 pts.</span>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_desc">
                                    <span class="title">Lorem Ipsum dolor sit</span>
                                    <p>
                                       Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    </p>
                                </div>
                                
                            </li>
                            
                            <li  class="item">
                                <div class="fvrts_img uk-clearfix" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                                    <img src="assets/img/images/rdm_img_03.jpg"/>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_pts">
                                    <span>150 pts.</span>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_desc">
                                    <span class="title">Lorem Ipsum dolor sit</span>
                                    <p>
                                       Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    </p>
                                </div>
                                
                            </li>
                            
                            <li  class="item">
                                <div class="fvrts_img uk-clearfix" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                                    <img src="assets/img/images/rdm_img_03.jpg"/>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_pts">
                                    <span>150 pts.</span>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_desc">
                                    <span class="title">Lorem Ipsum dolor sit</span>
                                    <p>
                                       Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    </p>
                                </div>
                                
                            </li>
                            
                            <li  class="item">
                                <div class="fvrts_img uk-clearfix" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                                    <img src="assets/img/images/rdm_img_03.jpg"/>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_pts">
                                    <span>150 pts.</span>
                                    <button><i class="uk-icon-heart"></i></button>
                                </div>
                                <div class="fvrts_desc">
                                    <span class="title">Lorem Ipsum dolor sit</span>
                                    <p>
                                       Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    </p>
                                </div>
                                
                            </li>
                            </div>
                        </ul>
                    </div>
                    
                    <div class="uk-width-1 uk-text-center">
                        <ul class="paginate">
                            <li>
                                <a class="fvrtprev">Prev</a>
                            </li>
                            <li>
                                <a class="fvrtnext">Next</a>
                            </li>
                        </ul>
                    </div>
            
                    
                </div>
            
            
            
            <div class="uk-width-1 accpd">
            <h4 style="color: #02b1c6">CATALOGUE</h4>
           
                        
                        <div class="rdmacc uk-clearfix">
            <div id="rdm" class="owl-carousel owl-theme">
                <div class="rdm_cont">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                
                <div class="rdm_cont">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                            
                <div class="rdm_cont">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                            
                <div class="rdm_cont">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                
                <div class="rdm_cont">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                
                <div class="rdm_cont">
                    <div class="rdm_img" style="background: url('assets/img/images/rdm_img_03.jpg') center / cover no-repeat">
                        <img src="assets/img/images/rdm_img_03.jpg"/>
                        <div class="rdm-scmd">
                            <ul>
                                <li class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-facebook"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-twitter"></i></a>
                                </li>
                                <li  class="uk-vertical-align">
                                    <a href="#" target="_blank" class="uk-vertical-align-middle"><i class="uk-icon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="rdm_pts">
                        <span>150 pts.</span>
                        <button><i class="uk-icon-heart"></i></button>
                    </div>
                    <div class="rdm_desc">
                        <span class="title">Lorem Ipsum dolor sit</span>
                        <p>Lorem ipsum dolor sit Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                
                
                 
                 </div>
                
                <div class="uk-width-1 uk-text-center">
                <ul class="paginate">
                    <li>
                        <a class="btn rdmprev">Prev</a>
                    </li>
                    <li>
                        <a class="btn rdmnext">Next</a>
                    </li>
                </ul>
            </div>
                
            </div>
            
            </div>
            
            

            
        </div>    
    </div>
</div>


            
<?php include 'footer.php'; ?>