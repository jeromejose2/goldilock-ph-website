<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>gTizen |</title>
    <script type="text/javascript" src="assets/vendors/jquery-2.1.4.min.js"></script>
    <!--<link href="assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="assets/vendors/owlcarousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/vendors/owlcarousel/owl.theme.css" rel="stylesheet">
    <link href="assets/vendors/uikit/css/uikit.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/main.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
<script src='https://www.google.com/recaptcha/api.js'></script>

</head>

<body>
    <!--[if lt IE 10]>
  <p class="browsehappy">
    You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve
    your experience.
  </p>
<![endif]-->
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1608852356037646',
          xfbml      : true,
          version    : 'v2.3'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="uppr_nav">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-2 scl_icn">
                    <ul>
                        <li class="uk-vertical-align">
                            <a href="https://www.facebook.com/GoldilocksPH" target="_blank">
                                <i class="uk-icon-facebook uk-vertical-align-middle"></i>
                            </a>
                        </li>
                        <li class="uk-vertical-align">
                            <a href=" https://twitter.com/goldilocksph" target="_blank">
                                <i class="uk-icon-twitter  uk-vertical-align-middle"></i>
                            </a>
                        </li>
                        <li class="uk-vertical-align">
                            <a href="https://instagram.com/goldilocksph/" target="_blank">
                                <i class="uk-icon-instagram  uk-vertical-align-middle"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-1-2 usr_cnt">
                    <span class="uk-visible-large"><a href="myaccount.php"> hello, jessie aspiras</a>
                </span>
                    <a href="" class="bwn-btn">sign out</a>
                    <!-- for mobile -->
                    <span class="uk-hidden-large">
                    <span><a class="tog-acc"> hello, jessie aspiras &nbsp;<i class="uk-icon-caret-down uk-hidden-large"></i></a>
                    </span>
                    <ul class="tog-acc_menu">
                        <li><a href="myaccount.php">My Account</a></li>
                        <li><a href="">Sign Out</a></li>
                    </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="lwr_nav">
        <div class="uk-container uk-clearfix">
            <div class="lg_cont">
                <a href="index.php">
                    <img src="assets/img/images/gtizen_logo_03.png" />
                </a>
            </div>
            <div class="nav_cont">
                <nav class="uk-navbar">
                    <a class="uk-navbar-toggle uk-hidden-large uk-float-right"></a>
                    <ul class="uk-navbar-nav">
                        <li class="active"><a href="aboutus.php">About gtizen</a></li>
                        <li class=""><a href="privileges.php">privileges</a></li>
                        <li class=""><a href="howtobemember.php">become a member</a></li>
                        <li class=""><a href="redeem.php">redeem</a></li>
                        <li class=""><a href="mywallet.php">my wallet</a></li>
                        <li class=""><a href="promos.php">promos</a></li>
                        <li class=""><a href="contactus.php">contact us</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
