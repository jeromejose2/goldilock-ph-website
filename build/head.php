	<?php
		$assets_url = "assets/";
	?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
	<meta name="viewport" content="width=device-width"/>
    <meta name="copyright" content=""/>
	<meta name="description" content=""/>
	<meta name="keywords" content=""/>
	<meta name="robots" content=""/>

	<title> gTizen | </title>

	<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
   <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>-->
	<link rel="stylesheet" type="text/css" href="assets/css/uikit.min.css">
	<script type="text/javascript" src="assets/js/uikit.min.js"> </script>
    <script type="text/javascript" src="assets/js/components/accordion.min.js"> </script>

    <link rel="stylesheet" type="text/css" href="assets/owlcarousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/owlcarousel/owl.theme.css">
	<script type="text/javascript" src="assets/owlcarousel/owl.carousel.js"> </script>
    <script type="text/javascript" src="assets/owlcarousel/owl.carousel.min.js"> </script>
    
    <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="assets/css/gtizen_styles.css">
    <link rel="stylesheet" type="text/css" href="assets/css/gtizen_responsive.css">
</head>

<body>

<div class="uppr_nav">
	<div class="uk-container">
		<div class="uk-grid">
		
			<div class="uk-width-1-2 scl_icn">
                <ul>
				<li class="uk-vertical-align"><a href="#" target="_blank">
					<i class="uk-icon-facebook uk-vertical-align-middle"></i>
				</a></li>
				<li class="uk-vertical-align"><a href="#" target="_blank">
					<i class="uk-icon-twitter  uk-vertical-align-middle"></i>
				</a></li>
				<li class="uk-vertical-align"><a href="#" target="_blank">
					<i class="uk-icon-instagram  uk-vertical-align-middle"></i>
				</a></li>
                    </ul>
			</div>
			
			<div class="uk-width-1-2 usr_cnt">
				<span class="uk-visible-large"><a href="myaccount.php"> hello, jessie aspiras</a>
                </span>
				<a href="" class="bwn-btn">sign out</a>
                
                <!-- for mobile -->
                <span  class="uk-hidden-large">
                    <span><a class="tog-acc"> hello, jessie aspiras &nbsp;<i class="uk-icon-caret-down uk-hidden-large"></i></a>
                    </span>

                    <ul class="tog-acc_menu">
                        <li><a href="myaccount.php">My Account</a></li>
                        <li><a href="">Sign Out</a></li>
                    </ul>
                </span>
			</div>
			
		</div>
	</div>
</div>

<div class="lwr_nav">
	<div class="uk-container uk-clearfix">
		<div class="lg_cont">
		<a href="index.php">
            <img src="assets/img/images/gtizen_logo_03.png"/>
        </a>
		</div>
		<div class="nav_cont">
			<nav class="uk-navbar">
             <a class="uk-navbar-toggle uk-hidden-large uk-float-right"></a>
				<ul class="uk-navbar-nav">
					<li class="active"><a href="aboutus.php">About gtizen</a></li>
					<li><a href="privileges.php">privilages</a></li>
					<li><a href="howtobemember.php">become a member</a></li>
					<li><a href="redeem.php">redeem</a></li>
					<li><a href="mywallet.php">my wallet</a></li>
					<li><a href="promos.php">promos</a></li>
					<li><a href="contactus.php">contact us</a></li>
				</ul>
			</nav>
		</div>
	</div>
</div>

    



