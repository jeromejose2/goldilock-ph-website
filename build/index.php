<?php include 'header.php'; ?>

<div class="sldr_cont">

<div class="uk-clearfix">
    
      
  <div id="home-slider" class="owl-carousel owl-theme gld_slider">
    <div class="item" style="background-image: url('assets/img/images/slider_bg_05.jpg');">
      <div class="gld_sliderCont">
        <div class="uk-width-1 gld_sld">
            <p>Sign up for the Goldilocks Gtizen Card and get the golden treatment that you deserve!<br> Use your card to get rewards in Goldilocks stores nationwide, and as a VISA debit card in other establishments all over the world.<br/>
              <a href="howtobemember.php" class="btn-slide">Become a Goldilocks Gtizen now! </a>
            </p>
        </div>
      </div>
    </div>

    <div class="item" style="background-image: url('assets/img/images/slider_bg_05.jpg');">
      <div class="gld_sliderCont">
        <div class="uk-width-1 gld_sld">
            <p>Sign up for the Goldilocks Gtizen Card and get the golden treatment that you deserve!<br> Use your card to get rewards in Goldilocks stores nationwide, and as a VISA debit card in other establishments all over the world.</p>
        </div>
      </div>
    </div>
  </div>
   
  <div class="customNavigation uk-vertical-align">
    <a class="btn prev"><i class="uk-icon-large uk-icon-chevron-left uk-vertical-align-middle"></i></a>
  </div>

  <div class="customNavigation right uk-vertical-align">
    <a class="btn next"><i class="uk-icon-large uk-icon-chevron-right uk-vertical-align-middle"></i></a>
  </div>
</div>   
   

</div>

    <div class="flwdth-ylw-up home" style="min-height: 260px;">


      <div class="uk-container" style="position: relative; margin-top: -125px;">
          <div class="uk-flex">
      		<div class="pnl uk-invisible" data-uk-scrollspy="{cls:'uk-animation-slide-left uk-invisible', delay:300, repeat: true}">
      			<div class="uk-text-center">
      				<img src="assets/img/images/goldi-icon_11.png" />
      			</div>
      			<h2 class="uk-text-center">About <img src="assets/img/images/gtizen-ico_03.png" /> </h2>
      			<p>
      			For only P150, you could enjoy the sweet benefits of being a Goldilocks Gtizen Card holder.
      			</p>
      			<a href="aboutus.php" class="bwn-btn uk-float-right">Read More</a> 
      		</div>
      		
      		<div class="pnl uk-invisible" data-uk-scrollspy="{cls:'uk-animation-slide-bottom uk-invisible', delay:600, repeat: true}">
      			<div class="uk-text-center">
      				<img src="assets/img/images/shop-icon_03.png" />
      			</div>
      			<h2 class="uk-text-center">Privileges</h2>
      			<p>
      			Earn points with every purchase. Get updates on Goldi promos, product launches, events, and activities.
      			</p>
      			<a href="privileges.php" class="bwn-btn uk-float-right">Read More</a> 
      		</div>
      		
      		<div class="pnl uk-invisible" data-uk-scrollspy="{cls:'uk-animation-slide-right uk-invisible', delay:900, repeat: true}">
      			<div class="uk-text-center">
      				<img src="assets/img/images/corp-icon_03.png" />
      			</div>
      			<h2 class="uk-text-center">How To Be A Member</h2>
      			<p>
      			Visit your nearest Goldilocks store. Fill out a GTizen Application Form.
      			</p>
      			<a href="howtobemember.php" class="bwn-btn uk-float-right">Read More</a> 
      		</div>
          </div>
      </div>

    </div>


<?php include 'footer.php'; ?>