<?php include 'header.php'; ?>

<div class="flwdth-ylw-dw">
	<div class="uk-container">
		<h1> My Wallet</h1>
	</div>
</div>


<div class="flwdth">
    <div class="uk-container">
        <div class="uk-width-1 rwng">
<div class="uk-accordion" data-uk-accordion>
    
            <h1 class="hd-rib uk-accordion-title">
                <span >GETTING STARTED</span><span></span>
            </h1>
            <div class="lstpd uk-accordion-content" style="margin-bottom: 40px;">
            <strong>1.  What is the Gtizen MyWallet Visa card?
</strong>
            
            <p>The Gtizen MyWallet Visa card is a reloadable multi-purpose prepaid stored value card which offers the flexibility of managing your day-to-day financial transactions with the added convenience of access to the worldwide Visa network. With this prepaid stored value card, you spend only what you have loaded in the card, thus, preventing you from overspending!
</p>
            
            <p>You can also use your Gtizen MyWallet Visa card to settle bills (i.e., Meralco, Globe/ Smart, credit cards, etc.) via cashless payments and may still perform usual transactions like an ATM card does.</p>
            
            <strong>2.  How do I avail the Gtizen MyWallet Visa card?
</strong>
            
            <p>	Just visit any Goldilocks Branches nationwide. Simply fill-out the enrollment form, present one (1) valid photo-bearing identification card and pay the registration fee amount of P300.00.</p>
            
            <strong>3.  How does my Gtizen MyWallet Visa card work?
</strong>
            <p>Because it is a prepaid card, spending is limited to the amount of money you load in the card, thus, preventing you from overspending. Plus, because it carries the Visa logo, you can use it anywhere Visa cards are accepted worldwide. Each time you use your card, the purchase amount is automatically deducted from your MyWallet Visa card balance.
</p>
            
            <strong>4.  Where can I use my Gtizen MyWallet Visa card?</strong>
            
            <p>Your MyWallet Visa card is accepted at over 70,000 Visa-accredited merchants and over 40,000 BancNet-affiliated POS merchants in the Philippines. You can also transact at over 30 million Visa.merchants and 1.8 million ATMs in more than 200 countries and territories. If you don’t see the logos, just ask “Do you accept Visa or BancNet cards?”
</p>
            
            <strong>5.  What transactions can I perform with the Gtizen MyWallet Visa card?
</strong>
            <p>Through the Gtizen MyWallet Visa card, you can perform the following transactions:
</p>
            <p>Draw Cash via RCBC, RCBC Savings Bank, BancNet, Megalink and Express Net ATMs nationwide and through Visa ATMs worldwide. Gtizen MyWallet Visa cardholders may likewise draw funds via RCBC Branch upon presentment of acceptable identification card.</p>
            <p>Inquire on Load Balance via RCBC, RCBC Savings Bank, BancNet, Megalink and Express Net ATMs nationwide and through Visa ATMs worldwide. Gtizen MyWallet Visa cardholders may likewise inquire funds via BancNet Online or even through RCBC Branch upon presentment of acceptable identification card.
</p>
            <p>Pay Bills via RCBC, RCBC Savings Bank and other BancNet ATMs or through BancNet Online.
</p>
            <p>Shop without cash via BancNet and Visa Point-of-Sale (POS) terminals, BancNet Online.</p>
            
            <p>Cash reload from Savings/ Checking account to MyWallet Card.</p>
            <p>Send money to your loved ones in the province or to any location within the Philippines.
</p>
            </div>
           <h1 class="hd-rib uk-accordion-title">
                <span>ONLINE PURCHASES</span><span></span>
            </h1> 
            <div class="lstpd uk-accordion-content" style="margin-bottom: 40px;">
            <strong>6.  Is there an initial load amount required to activate my Gtizen MyWallet Visa card?</strong>
            <p>Yes. But because of the generosity of Goldilocks, the Gtizen MyWallet Visa Card is already pre-loaded and can be used anytime upon received.</p>
            
            <strong>7.  Is there a required maintaining balance on the MyWallet Visa Card?</strong>
            
            <p>None. The cardholder may exhaust up to the last centavo of the card. However, a cardholder can only load up to P100,000.00 per month regardless of how many MyWallet card he/she has.</p>
            
            <strong>8.  Does a Gtizen MyWallet Visa card earn interest?
</strong>
            
            <p>The Gtizen MyWallet Visa card and the value stored therein is not a deposit account. Thus, it does not earn interest and is not insured with Philippine Deposit Insurance Corporation (PDIC).
</p>
            </div>
            <h1 class="hd-rib uk-accordion-title">
                <span class="test-class">Loading & Withdrawal</span><span></span>
            </h1> 
            <div class="lstpd uk-accordion-content" style="margin-bottom: 40px;">
             <strong>9.   Can my authorized representative withdraw over-the-counter on my behalf?</strong>
            
            <p>No. Only the cardholder can withdraw personally over-the-counter upon presentment of valid ID and the MyWallet Visa card.
</p>
            
            <strong>10.  Where can I load my Gtizen MyWallet Visa card?
</strong>
            
            <p>Loading of additional value to the Gtizen Visa Card may be done through the following channels:<br>
            RCBC – branch counters, ATMs<br>
Internet – BancNet Online<br>
ATMs – via Fund Transfer using BancNet, Megalink and Express Net ATMs<br>
RCBC AccessOne (Internet Banking) – Accredited loading partner establishments of RCBC such as LBC Express outlets worldwide
            </p>
            
            <strong>11.  How do I make a purchase with my Gtizen MyWallet Visa card?
</strong>
            
            <p>For Goldilocks purchase, just present your card to the cashier.</p>
            <p>For BancNet POS terminals: Key-in your PIN after the cashier swipes your card at the POS terminal;</p>
            <p>For Visa-accepting merchants: Sign on the merchant sales slip/s or key-in your PIN when you purchase.</p>
            <p>The purchase amount will automatically be deducted from your Gtizen Visa card balance. For international transactions, local currency of the place where the card is used shall be applied.</p>
            
            <strong>12.  What currency will be displayed / dispensed if I am transacting abroad?</strong>
            
            <p>The local currency of the country where the card is used shall be applied. The conversion rate to be used shall be determined by the Visa International.</p>
            
            <strong>13.  Is there a transaction charge each time I use my Gtizen MyWallet Visa Card?</strong>
            
            <p>There are no transaction fees for purchases made at any BancNet or Visa accredited merchants. However, a minimum charge will be deducted from your card every time you withdraw from any non-RCBC, BancNet, Megalink, Expressnet or other Visa member bank ATM terminals. In addition, a convenience fee of P10.00 will be charged every time you load your MyWallet Visa card regardless of the amount.
</p>
            
            <strong>14.  How long can I use my MyWallet Visa card?</strong>
            
            <p>
You can make purchases and earn points using your Gtizen MyWallet Visa card until the expiration date indicated on the face of the card. Once the card is expired, the account shall automatically be closed. Any remaining amount as of the expiration date can be claimed or transferred to a new MyWallet Visa card by visiting your branch of account.</p>
            
            <strong>15.  What if my card gets captured at an ATM machine abroad? What should I do?
</strong>
            
            <p>Card Policy for internationally issued Prepaid Card is to cut/destroy the card and it should no longer be returned to the issuing bank. Hence, if your MyWallet Visa card gets captured abroad, replacement card should be requested immediately.
</p>
            
            <strong>16.  What is the number to call in case of problems / complaints?</strong>
            
            <p>
            Any complaint / problem encountered can be reported to RCBC Customer Contact Center at 877-7222, Domestic Toll-free 1-800-10000-7222 and International Toll-Free + 800-8888-7222.
Or visit their website at <a href="http://www.rcbc.com/personalbanking_cashcard.php" target="_blank">http://www.rcbc.com/personalbanking_cashcard.php.</a>
</p>
            </div>
    
            <h1 class="hd-rib uk-accordion-title">
                <span>BILLS PAYMENT</span><span></span>
            </h1>
            <div class="lstpd uk-accordion-content" style="margin-bottom: 40px;">
            <p>Pay bills the easy way!</p>
            <p>With your RCBC MyWallet Visa Card, you can now pay your various bills at any RCBC, RCBC Savings Bank, other BancNet ATMs nationwide and through BancNet Online.</p>
            <p><strong>Just follow these simple steps at the ATM:</strong></p>
            
            <ol>
                <li>Insert your ATM card and enter your Personal Identification Number (PIN).
</li>
                <li>On the screen, select <br/>
Bills Payment <br/>
Category <br/>
Company <br/>
ex. (Utilities) > (Meralco)
</li>
                <li> Key in your billing Account number</li>
                <li>Choose the account from which your payment will be taken: <br/>
Savings Account <br/>
Checking Account <br/>
or <br/>
Enter the amount to be paid and confirm it. <br/>
0,000.00 <br/>
YES
</li>
            </ol>
            
            <p><strong>If you’re paying through BancNet Online, please follow the steps:
</strong></p>
            
            <ol>
                <li>Go to <a href="http://www.bancnetonline.com/" target="_blank">www.bancnetonline.com</a> , click on the RCBC logo.</li>
                <li>Read the agreement in using BancNet Online and click “I Agree” button if you agree with the Terms and Conditions.</li>
                <li>Click on “Bills Payment”.
                </li>
                <li>Fill out the required fields: • Biller/Institution • Subscriber Number • Bank Name • Card Number • Member Number • Account <br/>Type • Amount to be paid </li>
                <li>Enter your ATM PIN on the onscreen keypad.</li>
                <li>Click the Submit button.</li>
            </ol>
            </div>
            <h1 class="hd-rib uk-accordion-title">
                <span>cashless shopping</span><span></span>
            </h1> 
            <div class="lstpd uk-accordion-content" style="margin-bottom: 40px;">
            <small>(through Point-of-Sale or POS Terminals)</small>
            
            <p>Gtizen MyWallet Visa card allows you to shop even without touching the money in your pocket. That’s because when settling your bill at the counter, you can simply pay with your RCBC MyWallet Visa card at no extra charge!
</p>
            <p>It is accepted at over 40,000 BancNet-affiliated merchants nationwide and over 30 million Visa merchants worldwide so there’s no need to take the extra step of going to the ATM to withdraw. This service enables your merchant to automatically debit your account balance upon purchase of an item.</p>
            
            <p><strong>How to use Gtizen MyWallet Visa Card at POS Terminals:</strong></p>
            
            <ol>
            <li>  Shop anywhere in the Goldilocks Philippines.</li>
                <li>Present and pay with your Gtizen MyWallet Visa card.
</li>
                <li>At the counter, the cashier swipes your Gtizen MyWallet Visa card in the POS Terminal.</li>
                <li>This will electronically deduct the exact amount of your purchase from your Gtizen MyWallet Visa account.</li>
               
            </ol>
            
            <p>Note: Please ensure that your MyWallet Visa card balance covers the amount of your purchase, otherwise, transaction will be rejected.</p>
            
            
            </div>
        </div>
        </div>
    </div>
</div>


<?php include 'footer.php'; ?>
